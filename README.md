# Software Studio 2019 Spring Assignment_02

## Topic
* Project Name : Assignment_02 (Raiden)

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animation|10%|Y|
|Particle Systems|10%|Y|
|Sound effects|5%|Y|
|UI|5%|Y|
|Leaderboard|5%|Y|

## Bonus
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Multi-player|15%|N|
|Bullet automatic aiming|5%|Y|
|Unique bullet|5%|Y|
|Little helper|5%|N|
|Boss unique movement and attack-mode|5%|Y|

## Website Detail Description

# 作品網址：https://106000103.gitlab.io/Assignment_02

# Jucify mechanisms : 
1. Level: If the player gets more than 100 points, he/she can access the next level.
2. Skill: The player can use laser gun if he/she has enough MP(energy).

# Bonus : 
1. Unique bullet: In round 1, if the player gets the rocket coins, he/she can launch rockets.
2. Bullet automatic aiming: The rockets are automatically aiming. They will follow the trail of the closest enemy.

# Control
Press UP, DOWN, LEFT, RIGHT keys to move.
Press SPACE to shoot.
Press Z key to use laser gun (ultimate skill).
Press DOWN and SPACE to lunch rockets.